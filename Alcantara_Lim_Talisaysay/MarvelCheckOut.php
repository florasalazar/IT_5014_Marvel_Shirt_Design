<html>
<head>
    <title>Marvel Design A Shirt</title>
	<link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'/>
	<link rel='stylesheet' href="css/MarvelCart.css">
	<link rel='icon' href='img/favicon.ico'/>
	<script src='js/jquery.min.js'></script>
</head>

<style>
	#side{
		background-color: rgba(0,0,0,0.8);
		height: 590px;
		margin-left: 100px;
	}
	.col-md-11{
		background-color: rgba(246,246,246,0.6);
		height: 590px;
		width: 1050px;
		margin-right: 100px;
	}
	.items{
		margin: 20px;
	}
	#paymentG{
		margin: 20px;
		padding: 10px;
	}
	#checkoutDesign{
		background-color: rgba(0,0,0,0.8);
		height: 10px;
	}
</style>

<body>
<center>
	<div class='row'>
		<img class='text-center' src='img/marvellogo.png' style='width:300px;'>
	</div>
</center>
	<div class='row'>
		<div class='col-md-1' id='side'>
			
		</div>
		<div class='col-md-11'>
			<div class='row cssTotal paddingTotal' style='background-color:rgba(246,246,246,0.6);'>
	  			<b>CHECK OUT PROCESS | TRANSACTION SUMMARY</b>
	  		</div>
			<div class='items'>
				<div class='row'>
					<div class='col-md-3'>
						<img src='img/roundneck/sample1.png' style='width:150px;'> 
					</div>
					<div class='col-md-7'>
						<h3 style='text-transform: uppercase'><b>The Hulk Shirt</b></h3>
						<p>Size <b style='padding-left:35px'>M</b></p>
					    <p>Color <b style='padding-left:25px'>Green</b></p> 
						<p>Quantity <b style='padding-left:10px'>10 pcs.</b></p> 
					</div>
					<div class='col-md-2'>
						<br><br><br><br><br><br>
						<p style=' color:rgba(191,0,0)'> <b>Php 2500.00</b></p>
					</div>
				</div>
				<br><br>
				<div class='row'>
					<div class='col-md-3'>
						<img src='img/roundneck/sample2.png' style='width:150px;'>
					</div>
					<div class='col-md-7'>
						<h3 style='text-transform: uppercase'><b>Captain America Shirt</b></h3>
					    <p>Size <b style='padding-left:35px'>M</b></p>
					    <p>Color <b style='padding-left:25px'>Blue</b></p> 
					    <p>Quantity <b style='padding-left:10px'>5 pcs.</b></p>   
					</div>
					<div class='col-md-2'>
						<br><br><br><br><br><br>
						<p style=' color:rgba(191,0,0)'> <b>Php 1250.00</b> </p>
					</div>
				</div>
			</div>	
			<div class='row cssTotal paddingTotal' style='background-color:rgba(246,246,246,0.6);'>
	  			<b>CHECK OUT PROCESS | PAYMENT GUIDE</b>	
		  		<div class='row' id="paymentG">
		  			<div class='col-md-5'>
		  				<p>Grand Total <b style='padding-left:35px'>3750.00</b></p>
				    	<p>Transaction Status <b style='padding-left:25px'>Paid</b></p>  
		  			</div>
				    <div class='col-md-7'>
				    	<div class='row text-center'>
				  			<a class='btn btnMarvelCart chckoutMarvelCart'>PROCEED</a>
			  			</div>
				    </div>
		  		</div>
		  		<div id="checkoutDesign"></div>
	  		</div>	
		</div>
	</div>
</body>
</html>