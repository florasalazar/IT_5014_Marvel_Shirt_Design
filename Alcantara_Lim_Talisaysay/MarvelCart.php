<html>
<head>
	<link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'>
	<link rel='stylesheet' href="css/MarvelCart.css">
	<script src="bootstrap/js/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>

	<title>Dashboard</title>
</head>
<body>
	<div class="row">
	  <div class='col-md-12' style='margin-bottom:10px; padding-left:20px; color:white'>
	  	<p style='float:left; padding-right:10px; padding-top:10px'> 
	  		<a class='btn btnMarvelCart backMarvelCart'><span class='glyphicon glyphicon-menu-left'></span></a> 
	  	</p>
	  	<center><p style='padding-right:10px'> 
	  		<img class='text-center' src='img/marvellogo.png' style='width:300px;'> 
	  	</center>
	  	</p>
	  </div>
	  <?php 
	 //  	if(isset($_GET['pid'])){
		// $ret = mysqli_query($mysqli, "DELETE FROM product WHERE product_id =".$_GET['pid']);
		// 	if($ret){
		// 		echo "<div class='row del-pop'>";
		//         echo "<div class='col-md-4 col-md-offset-4 text-center' style=' color:#07a7e3;
		//                                                                             border:1px solid #07a7e3;
		//                                                                             padding:20px 30px 20px 20px;
		//                                                                             border-radius:10px;
		//                                                                             margin-bottom:20px;
		//                                                                           '>";
		//         echo "<span class='glyphicon glyphicon-trash' style='margin-right:10px;'></span>";
		//         echo "Product <b>".$_GET['prodname']."</b> has been Removed!";
		//         echo "</div>";
		//         echo "</div>";
		// 	}
		// }else if(isset($_POST['pname']) && isset($_POST['pqty']) && isset($_POST['pprice'])){
		// 	echo "<div class='row del-pop'>";
		//     echo "<div class='col-md-4 col-md-offset-4 text-center' style=' color:#07a7e3;
		//                                                                     border:1px solid #07a7e3;
		//                                                                     padding:20px 30px 20px 20px;
		//                                                                     border-radius:10px;
		//                                                                     margin-bottom:20px;
		//                                                                   '>";
		//     echo "<span class='glyphicon glyphicon-ok' style='margin-right:10px;'></span>";
		//     echo "Product <b>".$_POST['pname']."</b> has been Added!";
		//     echo "</div>";
		//     echo "</div>";
		// }

	   ?>
	  <div class="row">
	  	<div class="col-md-7 col-md-offset-1">
	  		<div class='row' style='background-color:rgba(0,0,0,0.8); color:white; margin-bottom:5px; padding-left:10px; padding-top:10px; padding-bottom: 10px'> 
	  			Marvel Cart
	  			<p style='float:right; padding-right:10px'> Total Item (2) </p>
	  		</div>
	  		<div class='tshirtMarvelCart'>
		  		<div class='row marvelCart1' style='margin-bottom:5px; padding:10px; background-color:rgba(246,246,246,0.6)'>
		  			<p style='float:left; padding-right:10px; padding-left:20px'> 
		  				<img src='img/roundneck/sample1.png' style='width:150px;'> 
		  			</p>
		  			<p style='float:right; padding-right:10px;'> 
		  				<a class='btn removeMarvelCart removeMarvelCart1' alt='1' style='color:#bf0404; font-size:25px'>
		  					<span class='glyphicon glyphicon-remove'></span>
		  				</a>
		  			</p>
		  			<div>
					  <h3 class='cartName1' style='text-transform: uppercase'><b>The Hulk Shirt</b></h3>
					  <p>Size <b style='padding-left:35px'>M</b></p>
					  <p>Color <b style='padding-left:25px'>Green</b></p> 
					  <p>Quantity 
					  	<b style='padding-left:10px' class='qty1'>10 pcs.</b> 
					  	<button type="button" class="btn btn-primary editCartItem editCartItem1" alt='1' style='padding:4px 6px;'>
					  		<span class='glyphicon glyphicon-plus'/>
					  	</button>
					  	<span class='edit-confirm' id='confirm1' style='padding-left:10px' hidden>
					  		<input class='editQuantity1' type='number' value='10' min='0' style="width: 3.5em"> pcs. 
					  		<span>
	                  			<!-- <a href='products.php?pid=".$row['product_id']."&&prodname=".$row['product_name']."'> -->
	                  			<!-- <a href=''> -->
		                  			<button class='btn btn-success confirmEdit confirmEdit1' alt='1' style='padding:4px 6px;'>
		                  				<span class='glyphicon glyphicon-ok'></span>
		                  			</button>
	                  			<!-- </a> -->
	                  		</span>
	                  		<span>
	                  			<!-- <a href=''> -->
		                  			<button class='btn btn-danger cancelEdit cancelEdit1' alt='1'  style='padding:4px 6px;'>
		                  				<span class='glyphicon glyphicon-remove'></span>
		                  			</button>
	                  			<!-- </a> -->
	                  		</span>
	                  	</span>
					  </p>  
					  <p style='float:right; padding-right:20px; color:rgba(191,0,0)'> 
					  	<b>Php 250.00</b> / pax 
					  </p>  
					  <!-- <input type="number" value="10" min="0" style="width: 3.5em"> pcs.  -->
					</div>
		  		</div>

		  		<div class='row marvelCart2' style='margin-bottom:5px; padding:10px; background-color:rgba(246,246,246,0.6)'>
		  			<p style='float:left; padding-right:10px; padding-left:20px'> 
		  				<img src='img/roundneck/sample2.png' style='width:150px;'> 
		  			</p>
		  			<p style='float:right; padding-right:10px;'> 
		  				<a class='btn removeMarvelCart removeMarvelCart2' alt='2' style='color:#bf0404; font-size:25px'>
		  					<span class='glyphicon glyphicon-remove'></span>
		  				</a>
		  			</p>
		  			<div>
					  <h3 class='cartName2' style='text-transform: uppercase'><b>Captain America Shirt</b></h3>
					  <p>Size <b style='padding-left:35px'>M</b></p>
					  <p>Color <b style='padding-left:25px'>Blue</b></p> 
					  <p>Quantity 
					  	<b style='padding-left:10px' class='qty2'>5 pcs.</b> 
					  	<button type="button" class="btn btn-primary editCartItem editCartItem2" alt='2' style='padding:4px 6px;'>
					  		<span class='glyphicon glyphicon-plus'/>
					  	</button>
					  	<span class='edit-confirm' id='confirm2' style='padding-left:10px' hidden>
					  		<input class='editQuantity2' type="number" value="5" min="0" style="width: 3.5em"> pcs. 
					  		<span>
	                  			<!-- <a href='products.php?pid=".$row['product_id']."&&prodname=".$row['product_name']."'> -->
	                  			<!-- <a href=''> -->
		                  			<button class='btn btn-success confirmEdit confirmEdit2' alt='2' style='padding:4px 6px;'>
		                  				<span class='glyphicon glyphicon-ok'></span>
		                  			</button>
	                  			<!-- </a> -->
	                  		</span>
	                  		<span>
	                  			<!-- <a href=''> -->
		                  			<button class='btn btn-danger cancelEdit cancelEdit2' alt='2'  style='padding:4px 6px;'>
		                  				<span class='glyphicon glyphicon-remove'></span>
		                  			</button>
	                  			<!-- </a> -->
	                  		</span>
	                  	</span>
					  </p>    
					  <p style='float:right; padding-right:20px; color:rgba(191,0,0)'> 
					  	<b>Php 250.00</b> / pax 
					  </p> 
					  <!-- <input type="number" value="5" min="0" style="width: 3.5em"> pcs. -->
					</div>
		  		</div>

	  		</div>
	  		<!-- <div class='row' style='background-color:rgba(0,0,0,0.8); color:white; margin-top:5px; margin-bottom:5px; padding-left:10px; padding-top:10px; padding-bottom: 10px'> 
	  		</div> -->
	  	</div>
	  	<div class="col-md-3" style='margin-left:20px;'>
	  		<div class='row' style='background-color:rgba(0,0,0,0.8); padding-top:10px; padding-bottom:10px; margin-bottom:5px'> 
	  		</div>
	  		<div class='row cssTotal paddingTotal' style='background-color:rgba(246,246,246,0.6);'>
	  			SUBTOTAL
	  			<p style='float:right; padding-right:10px'> Php 500.00 </p>
	  		</div>
	  		<div class='row cssTotal paddingTotal' style='background-color:rgba(246,246,246,0.6); padding-top:20px; padding-bottom:20px'>
	  			<b>GRAND TOTAL</b>
	  			<p style='float:right; padding-right:10px'><b> Php 500.00 </b></p>
	  			<div class='row text-center' style='margin-bottom:10px; margin-top:50px'>
		  			<a href="MarvelCheckOut.php" class='btn btnMarvelCart chckoutMarvelCart'>GO TO CHECKOUT</a>
	  			</div>
	  			<div class='row text-center'>
		  			<a class='btn btnMarvelCart designMarvelCart'>CONTINUE DESIGNING</a>
	  			</div>
	  		</div>
	  	</div>
	  </div>
	</div>
</body>

</html>

<script>
	$(".editCartItem").click(function () {
	  var ind = $(this).attr("alt");
	  $("#confirm"+ind).show();
	  $(".qty"+ind).hide();
	  $(".editCartItem"+ind).hide();
	});

	$(".cancelEdit").click(function () {
	  var x = $(this).attr("alt");
	  $("#confirm"+x).hide();
	  $(".qty"+x).show();
	  $(".editCartItem"+x).show();
	});

	$(".confirmEdit").click(function () {
	  var x = $(this).attr("alt");
	  var cartName = $(".cartName"+x).text();
	  alert(cartName + " has been updated!");
	  $("#confirm"+x).hide();
	  $(".qty"+x).show();
	  $(".editCartItem"+x).show();
	});

	$(".removeMarvelCart").click(function(){
		var x = $(this).attr("alt");
		var cartName = $(".cartName"+x).text();
		confirm("Do you want to remove "+cartName+"?");
		$(".marvelCart"+x).hide();
	});

	// setTimeout(function() {
	//   $(".del-pop").fadeOut()
	// }, 2000);
</script>